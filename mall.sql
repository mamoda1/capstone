-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2021 at 11:36 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mall`
--

-- --------------------------------------------------------

--
-- Table structure for table `accept_vendor`
--

CREATE TABLE `accept_vendor` (
  `v_id` int(5) NOT NULL,
  `v_email` varchar(20) NOT NULL,
  `v_name` varchar(20) NOT NULL,
  `v_pass` varchar(20) NOT NULL,
  `v_image` text NOT NULL,
  `username` varchar(20) NOT NULL,
  `mobile` int(5) NOT NULL,
  `address` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `accept_vendor`
--

INSERT INTO `accept_vendor` (`v_id`, `v_email`, `v_name`, `v_pass`, `v_image`, `username`, `mobile`, `address`) VALUES
(1, 'acowsby0@last.fm', 'Jaxnation', '8hKnLlM', 'http://dummyimage.com/125x213.png/5fa2dd/ffffff', 'hgalilee0', 31, '378 Fulton Junction'),
(2, 'smushet1@aboutads.in', 'Devshare', 'IihV9ebYiG', 'http://dummyimage.com/224x244.bmp/dddddd/000000', 'atease1', 86, '2803 Oak Plaza'),
(3, 'kchessun2@msu.edu', 'Thoughtmix', 'WW3j4Y2jR', 'http://dummyimage.com/244x111.jpg/dddddd/000000', 'wshoard2', 64, '1 4th Pass'),
(4, 'hmerrikin3@imageshac', 'Kanoodle', 'g5VnaNF', 'http://dummyimage.com/248x214.jpg/5fa2dd/ffffff', 'irevel3', 30, '17422 Fallview Way'),
(5, 'moertzen4@mediafire.', 'Riffpath', 'sVhyaXGlww', 'http://dummyimage.com/170x137.png/ff4444/ffffff', 'broycroft4', 33, '23 Vernon Park'),
(6, 'bstave5@nba.com', 'Browsetype', 'uSdQ8u', 'http://dummyimage.com/148x147.png/ff4444/ffffff', 'nmoyer5', 86, '3 Stuart Point'),
(7, 'afarran6@networkadve', 'Fatz', '00b47LmWRU', 'http://dummyimage.com/241x178.jpg/dddddd/000000', 'srawet6', 86, '3893 Green Ridge Cen'),
(8, 'iaskin7@twitter.com', 'Topicware', 'LEmBV1', 'http://dummyimage.com/152x114.png/ff4444/ffffff', 'eglacken7', 252, '40 Hagan Center'),
(9, 'pmcrannell8@hatena.n', 'Dynabox', 'csSgxHVjv', 'http://dummyimage.com/101x210.png/dddddd/000000', 'jkayser8', 86, '3106 Scoville Place'),
(10, 'mmerchant9@tinypic.c', 'Geba', 'JrUv01Mz', 'http://dummyimage.com/199x153.jpg/dddddd/000000', 'swoakes9', 237, '10 Bartelt Court'),
(66, 'mamodaobeidat1@gmail', 'MAHMOUD', 'mmmmm', 'christian-wiediger-70ku6P7kgmc-unsplash.jpg', 'muki@dff', 12221, 'aaa'),
(67, 'pinky@gmail.com', 'DATABASE', 'nnnjh', 'christian-wiediger-70ku6P7kgmc-unsplash.jpg', 'muki@dff', 12221, 'jordan'),
(68, 'pinky@gmail.com', 'MAHMOUD', ',llmml', 'kote-puerto-ILQoiHMJMME-unsplash.jpg', 'muki@dff', 12221, 'jordan');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(5) NOT NULL,
  `admin_email` varchar(20) NOT NULL,
  `admin_name` varchar(20) NOT NULL,
  `admin_pass` varchar(20) NOT NULL,
  `admin_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_email`, `admin_name`, `admin_pass`, `admin_image`) VALUES
(7, 'mamodaobeidat1@gmail', 'mahmoud ahmad', '123', 'Hydrangeas.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cat_id` int(5) NOT NULL,
  `cat_name` varchar(20) NOT NULL,
  `cat_desc` varchar(50) NOT NULL,
  `cat_image` text NOT NULL,
  `vendor_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `cat_name`, `cat_desc`, `cat_image`, `vendor_id`) VALUES
(47, 'Laptops', 'Lorem ipsum dolor sit amet, consectetur', 'sl.JPG', 3),
(48, 'Laptops', 'Lorem ipsum dolor sit amet, consectetur', 'ap.JPG', 2),
(49, 'Laptops', 'Lorem ipsum dolor sit amet, consectetur', 'hl.JPG', 4),
(50, 'Mobiles', 'Lorem ipsum dolor sit amet, consectetur', 'IP.JPG', 2),
(51, 'Mobiles', 'Lorem ipsum dolor sit amet, consectetur', 'sm.JPG', 3),
(52, 'Mobiles', 'Lorem ipsum dolor sit amet, consectetur', 'hph.JPG', 4),
(53, 'Tablets', 'Lorem ipsum dolor sit amet, consectetur', 'Ipad.JPG', 2),
(54, 'Tablets', 'Lorem ipsum dolor sit amet, consectetur', 'st.JPG', 3),
(55, 'Tablets', 'Lorem ipsum dolor sit amet, consectetur', 'ht1.JPG', 4),
(56, 'Friends', 'Lorem ipsum dolor sit amet, consectetur', '', 9),
(57, 'Family', 'Lorem ipsum dolor sit amet, consectetur', '', 9),
(58, 'Lonely ', 'Lorem ipsum dolor sit amet, consectetur', '', 9),
(59, 'Friends', 'Lorem ipsum dolor sit amet, consectetur', '', 10),
(60, 'Family', 'Lorem ipsum dolor sit amet, consectetur', '', 10),
(61, 'Lonely', 'Lorem ipsum dolor sit amet, consectetur', '', 10),
(62, 'Men', 'Lorem ipsum dolor sit amet, consectetur', 'mz.JPG', 5),
(63, 'Women', 'Lorem ipsum dolor sit amet, consectetur', 'wz.JPG', 5),
(66, 'Kids', 'Lorem ipsum dolor sit amet, consectetur', 'kz.JPG', 5),
(67, 'Men', 'Lorem ipsum dolor sit amet, consectetur', 'nm.jpg', 6),
(68, 'Women', 'Lorem ipsum dolor sit amet, consectetur', 'wm.jpg', 6),
(70, 'Men', 'Lorem ipsum dolor sit amet, consectetur', '', 8),
(71, 'Women', 'Lorem ipsum dolor sit amet, consectetur', '', 8),
(72, 'Kids', 'Lorem ipsum dolor sit amet, consectetur', '', 8),
(73, 'Boys', 'Lorem ipsum dolor sit amet, consectetur', '', 8),
(74, 'Men', 'Lorem ipsum dolor sit amet, consectetur', 'nm.jpg', 7),
(75, 'Women', 'Lorem ipsum dolor sit amet, consectetur', 'wm.jpg', 7),
(76, 'Kids', 'Lorem ipsum dolor sit amet, consectetur', 'nk.jpg', 7),
(77, 'Girls', 'Lorem ipsum dolor sit amet, consectetur', 'ng.jpg', 7),
(78, 'Men', 'Lorem ipsum dolor sit amet, consectetur', '', 21),
(79, 'Women', 'Lorem ipsum dolor sit amet, consectetur', '', 21),
(80, 'Boys', 'Lorem ipsum dolor sit amet, consectetur', '', 21),
(81, 'Men', 'Lorem ipsum dolor sit amet, consectetur', '', 23),
(82, 'Women', 'Lorem ipsum dolor sit amet, consectetur', '', 23),
(83, 'Girls', 'Lorem ipsum dolor sit amet, consectetur', '', 23),
(84, 'Men', 'Lorem ipsum dolor sit amet, consectetur', '', 25),
(85, 'Women', 'Lorem ipsum dolor sit amet, consectetur', '', 25),
(86, 'Kids', 'Lorem ipsum dolor sit amet, consectetur', '', 25),
(87, 'Men', 'Lorem ipsum dolor sit amet, consectetur', '', 26),
(88, 'Women', 'Lorem ipsum dolor sit amet, consectetur', '', 26),
(89, 'Girls', 'Lorem ipsum dolor sit amet, consectetur', '', 26),
(90, 'Men', 'Lorem ipsum dolor sit amet, consectetur', 'mw.JPEG', 1),
(91, 'Women', 'Lorem ipsum dolor sit amet, consectetur', 'ww.JPG', 1),
(92, 'Boys', 'Lorem ipsum dolor sit amet, consectetur', 'wc.JPG', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `c_id` int(11) NOT NULL,
  `c_name` varchar(20) NOT NULL,
  `c_email` varchar(20) NOT NULL,
  `c_pass` varchar(20) NOT NULL,
  `c_image` text NOT NULL,
  `mobile` int(5) NOT NULL,
  `address` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`c_id`, `c_name`, `c_email`, `c_pass`, `c_image`, `mobile`, `address`) VALUES
(1, 'MAHMOUD1', 'pinky@gmail.com', 'aass', 'FB_IMG_1555885462670.jpg', 1222155577, 'Amman'),
(2, 'Catherine', 'chorsburgh2@eepurl.c', '8tOXScEZOZ', 'http://dummyimage.com/239x109.bmp/cc0000/ffffff', 7, '06 Straubel Road'),
(4, 'Arri', 'acarey3@uiuc.edu', 'dAi5MzBym', 'http://dummyimage.com/181x169.png/dddddd/000000', 670, '827 Ludington Point'),
(5, 'Nancie', 'nlombardo4@nih.gov', 'nuHlzyPEW4', 'http://dummyimage.com/181x199.bmp/ff4444/ffffff', 351, '49 Merchant Hill'),
(6, 'Nicki', 'nkesper5@icq.com', 'B6Lsx56HD', 'http://dummyimage.com/167x206.png/dddddd/000000', 7, '52477 Mcbride Hill'),
(7, 'Shelba', 'svanderlinde6@vimeo.', '5TfvIs6YClW9', 'http://dummyimage.com/106x192.jpg/ff4444/ffffff', 61, '65156 Red Cloud Lane'),
(8, 'Harold', 'hpendry7@feedburner.', 'm9IzBJE', 'http://dummyimage.com/200x221.png/ff4444/ffffff', 55, '748 Killdeer Avenue'),
(9, 'Margaretta', 'mhanson8@amazon.de', 'F1BdnTjA', 'http://dummyimage.com/153x101.jpg/ff4444/ffffff', 63, '2295 Carioca Hill'),
(10, 'Kenton', 'kgoatman9@harvard.ed', 'giAm0uv', 'http://dummyimage.com/198x226.jpg/5fa2dd/ffffff', 234, '571 Farmco Avenue'),
(11, 'Jeffrey', 'jfeldmana@over-blog.', 'cGKjQngE', 'http://dummyimage.com/107x156.jpg/cc0000/ffffff', 47, '232 Logan Parkway'),
(12, 'Bent', 'bwalentab@liveintern', '01lCqP9Cu5', 'http://dummyimage.com/103x112.bmp/dddddd/000000', 62, '41003 Charing Cross '),
(13, 'Cyndie', 'claitec@google.fr', 'rakQypTDhbH', 'http://dummyimage.com/118x226.jpg/5fa2dd/ffffff', 86, '76812 6th Crossing'),
(14, 'Tades', 'tmorod@sitemeter.com', 'rkto3jp1ElH', 'http://dummyimage.com/140x234.jpg/5fa2dd/ffffff', 351, '357 Darwin Way'),
(15, 'Jamal', 'jfehnerse@fema.gov', 'USLZkW7iXCkG', 'http://dummyimage.com/217x220.jpg/5fa2dd/ffffff', 7, '0 Eagan Pass'),
(16, 'Fan', 'fitzcovichf@shutterf', 'FbOyti9xvTLz', 'http://dummyimage.com/231x171.jpg/cc0000/ffffff', 62, '74394 Anhalt Crossin'),
(17, 'Justina', 'jcowellg@phpbb.com', 'PcwD6MKcz', 'http://dummyimage.com/139x137.bmp/dddddd/000000', 81, '13118 Mayfield Point'),
(18, 'Mycah', 'mescotth@flickr.com', 'bu4W9t', 'http://dummyimage.com/188x233.bmp/cc0000/ffffff', 86, '8652 Badeau Avenue'),
(19, 'Prudence', 'pcairneyi@vimeo.com', 'MJ1YQ4iIyFw', 'http://dummyimage.com/189x187.png/ff4444/ffffff', 86, '0114 Continental Lan'),
(20, 'Irv', 'ihalwardj@un.org', 'VY41mho2sEI', 'http://dummyimage.com/179x242.png/cc0000/ffffff', 56, '65 Talmadge Junction'),
(21, 'Ulrika', 'ugozardk@woothemes.c', 'sjxHv8pSz', 'http://dummyimage.com/156x205.jpg/5fa2dd/ffffff', 81, '493 Bunting Pass'),
(22, 'Gerald', 'gjambrozekl@pbs.org', 'bq35a1JcO', 'http://dummyimage.com/132x133.jpg/cc0000/ffffff', 46, '6868 Hanson Point'),
(23, 'Krystal', 'kroswarnem@lycos.com', 'S9faah9c', 'http://dummyimage.com/233x170.bmp/ff4444/ffffff', 86, '473 Mandrake Street'),
(24, 'Camilla', 'cmcsperronn@kickstar', 'byZTOMfe', 'http://dummyimage.com/232x135.jpg/cc0000/ffffff', 62, '313 Talisman Pass'),
(25, 'Kevina', 'kmanolovo@hhs.gov', 'DTbKgoN', 'http://dummyimage.com/107x185.png/ff4444/ffffff', 62, '334 Weeping Birch Ce'),
(26, 'Karel', 'kjewellp@cnn.com', 'oF8LkUm', 'http://dummyimage.com/184x225.jpg/ff4444/ffffff', 420, '34 Gale Center'),
(27, 'Clement', 'clawsonq@de.vu', 'UczS7p3Gw', 'http://dummyimage.com/151x189.bmp/ff4444/ffffff', 48, '744 Chinook Drive'),
(28, 'Mendy', 'mgarciar@nature.com', 'UzZPjZOr9p1', 'http://dummyimage.com/241x158.png/ff4444/ffffff', 48, '1 Lotheville Crossin'),
(29, 'Leonerd', 'llangstraths@skyrock', 'Aw70MHqnPNP', 'http://dummyimage.com/119x237.jpg/dddddd/000000', 86, '70 Pond Street'),
(30, 'Dyana', 'dmaccostyt@scienceda', 'KbC0dGyNnHXX', 'http://dummyimage.com/204x227.bmp/cc0000/ffffff', 351, '1 Carioca Plaza'),
(31, 'Cornell', 'chambrooku@so-net.ne', 'lEU2olQCS9A3', 'http://dummyimage.com/217x130.png/5fa2dd/ffffff', 56, '23270 Sloan Avenue'),
(32, 'Osmond', 'oplowsv@home.pl', 'q2oJ1Ua34X', 'http://dummyimage.com/208x186.png/dddddd/000000', 86, '56 Marquette Plaza'),
(33, 'Dan', 'dsollomw@boston.com', 'PQSqXk', 'http://dummyimage.com/238x153.png/dddddd/000000', 63, '1 Golf Course Street'),
(34, 'Claudianus', 'cvescox@telegraph.co', 'hLtuLSCeDl', 'http://dummyimage.com/113x116.jpg/dddddd/000000', 963, '80 Fremont Crossing'),
(35, 'Mellie', 'mharbagey@furl.net', '1eSF7wA16Is', 'http://dummyimage.com/145x145.png/cc0000/ffffff', 62, '5971 Badeau Center'),
(36, 'Brier', 'bwicklinz@wiley.com', 'pU4CjwTQ', 'http://dummyimage.com/160x160.jpg/ff4444/ffffff', 420, '0478 Westerfield Way'),
(37, 'Randi', 'rolyfant10@gnu.org', 'jkCsY8plOH5s', 'http://dummyimage.com/234x156.bmp/5fa2dd/ffffff', 34, '69 Aberg Crossing'),
(38, 'Gleda', 'gcervantes11@godaddy', 'mQ3QlDk8l', 'http://dummyimage.com/188x151.bmp/ff4444/ffffff', 250, '2415 Raven Court'),
(39, 'Codi', 'cbartali12@google.pl', 'xxkoN4Qfm', 'http://dummyimage.com/184x165.bmp/cc0000/ffffff', 374, '48002 Mifflin Road'),
(40, 'Giusto', 'gmatthiae13@google.c', 'vnYPsmk', 'http://dummyimage.com/138x122.png/cc0000/ffffff', 48, '916 Mendota Drive'),
(41, 'Mathilda', 'mbollans14@weibo.com', 'Ayo0xUifj', 'http://dummyimage.com/162x215.bmp/5fa2dd/ffffff', 84, '50 Macpherson Place'),
(42, 'Janeczka', 'jmurra15@t-online.de', 'gSesvb', 'http://dummyimage.com/208x209.jpg/ff4444/ffffff', 62, '659 Susan Drive'),
(43, 'Cristen', 'cbrozek16@theguardia', 'OzIzYtI', 'http://dummyimage.com/231x142.bmp/ff4444/ffffff', 98, '206 Northwestern Jun'),
(44, 'Willow', 'wruf17@hud.gov', 'EtmV9IoeAbW', 'http://dummyimage.com/142x225.jpg/dddddd/000000', 98, '88861 Gina Crossing'),
(45, 'Byram', 'bjelphs18@globo.com', 'iZzG2GN5dPdj', 'http://dummyimage.com/226x162.png/ff4444/ffffff', 86, '82342 Delladonna Cen'),
(46, 'Brandy', 'bbridgett19@hugedoma', '6Rq8BWFsPSg6', 'http://dummyimage.com/139x218.jpg/5fa2dd/ffffff', 502, '70 Kropf Trail'),
(47, 'Irene', 'ibletsor1a@wufoo.com', '58sCVKuH5Z', 'http://dummyimage.com/205x183.png/cc0000/ffffff', 81, '282 Magdeline Point'),
(48, 'Osbert', 'ostollenberg1b@sitem', 'CZF4qb4N', 'http://dummyimage.com/162x180.bmp/cc0000/ffffff', 504, '31884 Autumn Leaf Po'),
(49, 'Durant', 'dkilgallen1c@a8.net', 'Xc0IBi1DWzIk', 'http://dummyimage.com/220x195.bmp/cc0000/ffffff', 261, '39 Lawn Center'),
(50, 'Golda', 'gcossans1d@seesaa.ne', 'lcsVjf', 'http://dummyimage.com/198x166.png/cc0000/ffffff', 54, '51 Hoepker Trail'),
(51, 'Gilberte', 'ggover1e@goo.gl', '5sqH8zKL', 'http://dummyimage.com/167x190.png/cc0000/ffffff', 94, '64 Forster Junction'),
(52, 'Heida', 'hmatthesius1f@about.', '3vNLNuqLm4S', 'http://dummyimage.com/138x100.bmp/ff4444/ffffff', 27, '23 Anderson Circle'),
(53, 'Marc', 'mlaming1g@jimdo.com', 'FuXy4k84W', 'http://dummyimage.com/198x149.png/ff4444/ffffff', 353, '87862 Susan Way'),
(54, 'Osbourn', 'okatzmann1h@wsj.com', 'GYNtFOpawzS', 'http://dummyimage.com/219x131.jpg/ff4444/ffffff', 420, '9464 Springs Circle'),
(55, 'Christiane', 'cscoular1i@technorat', 'ySnEvQ7De', 'http://dummyimage.com/171x239.png/dddddd/000000', 48, '9510 Towne Court'),
(56, 'Estella', 'etunna1j@list-manage', '0Dj4Pi7FPg2', 'http://dummyimage.com/154x154.jpg/5fa2dd/ffffff', 86, '367 Rieder Street'),
(57, 'Thaine', 'twynn1k@themeforest.', '7SvsNTQ1L', 'http://dummyimage.com/148x232.jpg/ff4444/ffffff', 48, '3 Hollow Ridge Road'),
(58, 'Friedrich', 'fwaldie1l@fc2.com', 'umjpmviCCBK', 'http://dummyimage.com/107x160.jpg/ff4444/ffffff', 358, '446 Rieder Lane'),
(59, 'Stafani', 'sbreznovic1m@miitbei', 'pkVcbReO', 'http://dummyimage.com/112x176.jpg/ff4444/ffffff', 62, '21 Scofield Court'),
(60, 'Tally', 'treside1n@soup.io', '4Zekpvv', 'http://dummyimage.com/210x199.png/dddddd/000000', 351, '980 Waywood Alley'),
(61, 'Peria', 'pmedway1o@mashable.c', 'LaF5bvQmLAz', 'http://dummyimage.com/223x121.bmp/dddddd/000000', 62, '195 Sunfield Hill'),
(62, 'Batsheva', 'bpigford1p@about.me', 'NROSaaJCl', 'http://dummyimage.com/239x154.bmp/5fa2dd/ffffff', 7, '4139 Lunder Alley'),
(63, 'Hermina', 'hrodenburg1q@hugedom', '0vXMJaN1a2A', 'http://dummyimage.com/174x142.bmp/ff4444/ffffff', 55, '37885 Myrtle Parkway'),
(64, 'Heindrick', 'hgrindrod1r@wootheme', 't4wBejRsYj', 'http://dummyimage.com/226x109.jpg/dddddd/000000', 55, '7936 Merchant Hill'),
(65, 'Minne', 'moleagham1s@dailymai', 'GVIHSy', 'http://dummyimage.com/135x189.jpg/dddddd/000000', 691, '45 Drewry Place'),
(66, 'Darin', 'dduffree1t@studiopre', '2IEWL7Tehp', 'http://dummyimage.com/217x180.bmp/dddddd/000000', 62, '11878 Sundown Circle'),
(67, 'Barb', 'bdoohan1u@unicef.org', 'CPGr5JfX', 'http://dummyimage.com/223x145.jpg/dddddd/000000', 46, '26 Comanche Hill'),
(68, 'Nathalie', 'ngreenmon1v@referenc', 'sfD4APc', 'http://dummyimage.com/156x170.png/ff4444/ffffff', 86, '4266 Graedel Street'),
(69, 'Martie', 'mgremain1w@deviantar', 'cgzemKViW', 'http://dummyimage.com/136x166.jpg/5fa2dd/ffffff', 48, '33 Carpenter Crossin'),
(70, 'Valencia', 'vkrout1x@wordpress.c', 'AfKbdQOZmXC8', 'http://dummyimage.com/197x167.png/dddddd/000000', 51, '3471 Boyd Street'),
(71, 'Joelynn', 'jsails1y@state.tx.us', 'FXzxg2soB', 'http://dummyimage.com/201x189.jpg/dddddd/000000', 86, '4874 Northport Park'),
(72, 'Jeannette', 'jmorde1z@nasa.gov', 'zvIxMaIQv', 'http://dummyimage.com/250x212.jpg/cc0000/ffffff', 46, '705 Memorial Pass'),
(73, 'Jeddy', 'jstorrie20@angelfire', 'n0y5t4gu', 'http://dummyimage.com/162x108.bmp/dddddd/000000', 48, '2 Derek Hill'),
(74, 'Ulrika', 'umillion21@state.tx.', 'I6BGHoSt', 'http://dummyimage.com/214x100.bmp/5fa2dd/ffffff', 380, '55109 Twin Pines Roa'),
(75, 'Kamilah', 'kheyns22@ft.com', 'JypQSH4c', 'http://dummyimage.com/204x233.png/5fa2dd/ffffff', 98, '1 Pine View Court'),
(76, 'Missy', 'mfortin23@w3.org', 'TLc6OkTWtx', 'http://dummyimage.com/180x144.jpg/dddddd/000000', 420, '07884 Ludington Alle'),
(77, 'Michell', 'mbahike24@psu.edu', 'GIe7hMXf9cr', 'http://dummyimage.com/113x148.jpg/ff4444/ffffff', 49, '341 American Ash Hil'),
(78, 'Mitchael', 'mflanaghan25@umich.e', 'lYosCvpk', 'http://dummyimage.com/166x185.jpg/5fa2dd/ffffff', 7, '38 Ronald Regan Way'),
(79, 'Maxy', 'mgravestone26@gov.uk', 'SkG8K2', 'http://dummyimage.com/229x178.bmp/dddddd/000000', 84, '865 Namekagon Avenue'),
(80, 'Aldwin', 'agunthorp27@rambler.', '54QzQYh2', 'http://dummyimage.com/232x249.bmp/dddddd/000000', 63, '4236 Summer Ridge Pl'),
(81, 'Sabrina', 'sboundey28@eventbrit', 'IMK8RJnjE', 'DSC_0028.JPG', 86, '5550 Sachs Hill'),
(82, 'Pearce', 'pausten29@is.gd', 'RPXta2c11my', 'http://dummyimage.com/188x212.png/5fa2dd/ffffff', 66, '6 Morning Court'),
(83, 'Elbert', 'eglaysher2a@about.me', 'zgNxtY', 'http://dummyimage.com/214x209.jpg/cc0000/ffffff', 380, '455 Portage Point'),
(84, 'Carin', 'ccrossby2b@spotify.c', 'ym9Im9b', 'http://dummyimage.com/136x236.bmp/cc0000/ffffff', 66, '3 Grasskamp Parkway'),
(85, 'Clayborne', 'cdrohan2c@indiatimes', 'xEU55kSX', 'http://dummyimage.com/166x142.png/ff4444/ffffff', 86, '828 Twin Pines Stree'),
(86, 'Susy', 'swateridge2d@oracle.', 'M7CmTE', 'http://dummyimage.com/196x190.jpg/cc0000/ffffff', 54, '393 Homewood Circle'),
(87, 'Torin', 'temmatt2e@diigo.com', 'URRPGNIgu', 'http://dummyimage.com/103x236.png/ff4444/ffffff', 86, '15 Birchwood Court'),
(88, 'Wilmar', 'wcagan2f@technorati.', 'KEaFFs', 'http://dummyimage.com/162x220.png/5fa2dd/ffffff', 33, '7029 Golf Street'),
(89, 'Pauly', 'pdark2g@goo.ne.jp', 'JfU43d', 'http://dummyimage.com/168x117.jpg/5fa2dd/ffffff', 995, '80 Buena Vista Lane'),
(90, 'Devondra', 'dpengilley2h@discove', 'n3n25Mhw8', 'http://dummyimage.com/176x240.jpg/dddddd/000000', 92, '5904 Bobwhite Terrac'),
(91, 'Alison', 'abrindle2i@yandex.ru', '4ACQoEgRGA7V', 'http://dummyimage.com/109x172.bmp/ff4444/ffffff', 380, '57 Trailsway Alley'),
(92, 'Beverley', 'bheatherington2j@sit', 'vx3vrXp', 'http://dummyimage.com/170x198.png/5fa2dd/ffffff', 880, '33461 Texas Lane'),
(93, 'Jillana', 'jwild2k@bluehost.com', 'AVQUj09Bs', 'http://dummyimage.com/242x176.bmp/5fa2dd/ffffff', 380, '3 Birchwood Terrace'),
(94, 'Bellanca', 'bparcell2l@tripadvis', 'TMds8H5', 'http://dummyimage.com/164x191.jpg/dddddd/000000', 33, '6 Fairfield Park'),
(95, 'Estelle', 'epetracci2m@nytimes.', 'PsmsAi4GVoW', 'http://dummyimage.com/210x128.png/5fa2dd/ffffff', 62, '55074 Portage Pass'),
(96, 'Hazel', 'hedes2n@studiopress.', 'DRn7Wa8K', 'http://dummyimage.com/204x239.jpg/cc0000/ffffff', 62, '01 Drewry Parkway'),
(97, 'Bartholomew', 'bpeggrem2o@seattleti', 'IWhulriKE0Ya', 'http://dummyimage.com/161x198.png/cc0000/ffffff', 86, '6 Coolidge Lane'),
(98, 'Eugenie', 'emacfarland2p@mysql.', 'zPfWO5US6FMC', 'http://dummyimage.com/214x116.bmp/dddddd/000000', 47, '4 Scofield Road'),
(99, 'Johna', 'jgoodlatt2q@typepad.', 'M9u9q3FRCo0r', 'http://dummyimage.com/163x178.bmp/cc0000/ffffff', 7, '653 Eastlawn Road'),
(100, 'Jobyna', 'jstockport2r@usgs.go', 'who17naJL', 'http://dummyimage.com/227x195.png/ff4444/ffffff', 504, '90947 Esch Hill'),
(101, 'mohammad', 'aaaa@gmail.com', 'mmsamds', 'Koala.jpg', 1454565, 'scsdcsdcdscs'),
(104, 'mahmoud', 'pinky@gmail.com1', 'sdasd', 'DSC_0028.JPG', 88877, 'cascsca'),
(105, 'mahmoud', 'pinky@gmail.com1', 'sdasd', 'DSC_0028.JPG', 88877, 'cascsca'),
(106, 'dfdsf', 'm@dsvds.com', 'sdsd', 'DSC_0030.JPG', 5448, '54475'),
(112, '6665', 'ssd', 'sadsd', '', 0, 'dsa'),
(113, 'MAHMOUD', 'pinky@gmail.com', 'asdfgh124', '', 1234567898, 'jordan');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `f_id` int(5) NOT NULL,
  `name` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `f_subject` varchar(20) NOT NULL,
  `feedback_text` text NOT NULL,
  `f_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`f_id`, `name`, `email`, `f_subject`, `feedback_text`, `f_date`) VALUES
(2, 'ahmad', 'mamodaobeidat1@gmail', 'Lorem ipsum ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', '2021-02-04'),
(3, 'ahmad', 'mamodaobeidat1@gmail', 'Lorem ipsum ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', '2021-02-04'),
(4, 'slameh', 'slameh@customer.com', ' Maecenas vulputate.', 'CONTACT INFO\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', '2021-02-04');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(5) NOT NULL,
  `v_id` int(5) NOT NULL,
  `pro_id` int(5) NOT NULL,
  `quantity` int(5) NOT NULL,
  `cat_id` int(5) NOT NULL,
  `order_num` int(10) NOT NULL,
  `order_date` date NOT NULL,
  `cust_id` int(5) NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `pro_id` int(5) NOT NULL,
  `pro_name` varchar(100) NOT NULL,
  `pro_desc` varchar(500) NOT NULL,
  `pro_image1` text NOT NULL,
  `pro_image2` text NOT NULL,
  `pro_image3` text NOT NULL,
  `v_id` int(5) NOT NULL,
  `cat_id` int(5) NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`pro_id`, `pro_name`, `pro_desc`, `pro_image1`, `pro_image2`, `pro_image3`, `v_id`, `cat_id`, `price`) VALUES
(26, 'Emporio Armani Sport', 'Lorem ipsum dolor sit amet, consectetur adipiscingLorem ipsum dolor sit amet, consectetur adipiscingLorem ipsum dolor sit amet, consectetur adipiscingLorem ipsum dolor sit amet, consectetur adipiscing', 'Emporio.JPG', 'Emporio.JPG', 'Emporio.JPG', 1, 90, 20),
(27, ' EMPORIO ARMANI WATCH', 'Lorem ipsum dolor sit amet, consectetur adipiscing', 'AR8034_l.JPG', 'AR8034_l.JPG', 'AR8034_l.JPG', 1, 90, 220),
(28, 'Armani Aviator Black', 'Lorem ipsum dolor sit amet, consectetur adipiscing', 'aviatorblack.JPG', 'aviatorblack.JPG', 'aviatorblack.JPG', 1, 90, 319),
(29, 'Rose-Gold', 'Lorem ipsum dolor sit amet, consectetur adipiscing', 'rose.JPG', 'rose.JPG', 'rose.JPG', 1, 91, 500),
(30, ' Armani Woman two-hand\'s', 'Lorem ipsum dolor sit amet, consectetur adipiscing', 'two_hands.JPG', 'two_hands.JPG', 'two_hands.JPG', 1, 91, 450),
(31, ' Armani Women\'s Stail', 'steel.JPGLorem ipsum dolor sit amet, consectetur adipiscing', 'steel.JPG', 'steel.JPG', 'steel.JPG', 1, 91, 120),
(32, 'Emporio Armani Men\'s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consecte', 'analog.JPG', 'analog.JPG', 'analog.JPG', 1, 92, 500),
(33, 'Emporio Armani 5a Watch', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consecte', '5a.JPG', '5a.JPG', '5a.JPG', 1, 92, 220),
(34, 'Armani Exchange Casual', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consecte', 'casual.JPG', 'casual.JPG', 'casual.JPG', 1, 92, 444),
(35, 'Buy 16-inch MacBook Pro', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consecte', 'ap1.JPG', 'ap1.JPG', 'ap1.JPG', 2, 48, 600),
(36, '13-inch MacBook Air - Space Gra', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consecte', 'ap2.JPG', 'ap2.JPG', 'ap2.JPG', 2, 48, 500),
(37, 'Apple\'s New MacBook Air With M1 Is a Huge Leap Forward', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consecte', 'ap3.JPG', 'ap3.JPG', 'ap3.JPG', 2, 48, 405),
(38, 'iPhone 11', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'iph1.JPG', 'iph1.JPG', 'iph1.JPG', 2, 50, 400),
(39, 'iPhone 12', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'iph2.JPG', 'iph2.JPG', 'iph2.JPG', 2, 50, 500),
(40, 'iPhone 11 pro max ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'iph3.JPG', 'iph3.JPG', 'iph3.JPG', 2, 50, 450),
(41, 'iPad Air 2019', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'ipad1.JPG', 'ipad1.JPG', 'ipad1.JPG', 2, 53, 500),
(42, 'iPad Air 16GB', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'ipad2.JPG', 'ipad2.JPG', 'ipad2.JPG', 2, 53, 321),
(43, 'iPad Air 2021', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'ipad3.JPG', 'ipad3.JPG', 'ipad3.JPG', 2, 53, 454),
(44, 'Samsung Galaxy S11', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'sam1.JPG', 'sam1.JPG', 'sam1.JPG', 3, 51, 545),
(45, 'Samsung Galaxy S12', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'sam2.JPG', 'sam2.JPG', 'sam2.JPG', 3, 51, 745),
(46, 'Samsung Galaxy Note 11', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'sam3.JPG', 'sam3.JPG', 'sam3.JPG', 3, 51, 445),
(47, 'Samsung-Galaxy Tab S6', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'stab1.JPG', 'stab1.JPG', 'stab1.JPG', 3, 54, 665),
(48, 'Samsung-Galaxy Tab S7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'stab2.JPG', 'stab2.JPG', 'stab2.JPG', 3, 54, 451),
(49, 'Samsung-Galaxy Tab S8', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'stab3.JPG', 'stab3.JPG', 'stab3.JPG', 3, 54, 325),
(50, 'Samsung Notebook 9 Pro', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'labs1.JPG', 'labs1.JPG', 'labs1.JPG', 3, 47, 500),
(51, 'Samsung Notebook 10 Pro', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'labs2.JPG', 'labs2.JPG', 'labs2.JPG', 3, 47, 800),
(52, 'Samsung Notebook 8 Pro', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'labs13.PNG', 'labs13.PNG', 'labs13.PNG', 3, 47, 410),
(53, 'HUAWEI Matebook X Pro 2020 - HUAWEI Global', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'labh1.JPG', 'labh1.JPG', 'labh1.JPG', 4, 49, 888),
(54, 'HUAWEI Matebook X Pro 2019 - HUAWEI Global', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'labh2.JPG', 'labh2.JPG', 'labh2.JPG', 4, 49, 920),
(55, 'HUAWEI Matebook X Pro 2021 - HUAWEI Global', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'labh3.JPG', 'labh3.JPG', 'labh3.JPG', 4, 49, 1200),
(56, 'CES 2019', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'taph1.JPG', 'taph1.JPG', 'taph1.JPG', 4, 55, 625),
(57, 'Huawei Midpad M5 ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'taph2.JPG', 'taph2.JPG', 'taph2.JPG', 4, 55, 455),
(58, 'Huawei Midpad M6', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'taph3.JPG', 'taph3.JPG', 'taph3.JPG', 4, 55, 600),
(59, 'Huawei P40', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'hm1.JPg', 'hm1.JPg', 'hm1.JPg', 4, 52, 744),
(60, 'Huawei Y9 Prime 2020', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'hm2.JPg', 'hm2.JPg', 'hm2.JPg', 4, 52, 499),
(61, 'Huawei Y7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'hm3.JPg', 'hm3.JPg', 'hm3.JPg', 4, 52, 120),
(62, 'VINTAGE SLIM FIT JEANS‏', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'zm1.JPG', 'zm1.JPG', 'zm1.JPG', 5, 62, 20),
(63, 'Shoes men 2019 ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'zm2.JPG', 'zm2.JPG', 'zm2.JPG', 5, 62, 30),
(64, 'zara men romania', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'zm3.JPG', 'zm3.JPG', 'zm3.JPG', 5, 62, 19.5),
(65, 'Kosciuszko ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'zw1.JPG', 'zw1.JPG', 'zw1.JPG', 5, 63, 80),
(66, ' black coat womens ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.\r\n', 'zw2.JPG', 'zw2.JPG', 'zw2.JPG', 5, 63, 50),
(67, 'cargo pants womens zara‏\r\n', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'zw3.JPG', 'zw3.JPG', 'zw3.JPG', 5, 63, 10),
(68, 'Toddler Couture ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'zk1.JPG', 'zk1.JPG', 'zk1.JPG', 5, 66, 15),
(69, 'zara children blouse ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'zk2.JPG', 'zk2.JPG', 'zk2.JPG', 5, 66, 18),
(70, 'DOUBLE BREASTED PLAID COAT ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'zk3.JPG', 'zk3.JPG', 'zk3.JPG', 5, 66, 12),
(71, 'T-shirt', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'cm1.JPG', 'cm1.JPG', 'cm1.JPG', 6, 67, 10),
(72, 'Jacket ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'cm2.JPG', 'cm2.JPG', 'cm2.JPG', 6, 67, 25),
(73, 'Shoes', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'cms.PNG', 'cms.PNG', 'cms.PNG', 6, 67, 15),
(74, 'Shoes', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'cw1.JPG', 'cw1.JPG', 'cw1.JPG', 6, 68, 20),
(75, 'Jacket', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'cw2.JPEG', 'cw2.JPEG', 'cw2.JPEG', 6, 68, 35),
(76, 'Pant', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'cw3.JPG', 'cw3.JPG', 'cw3.JPG', 6, 68, 25),
(77, 'Training suite  ', '                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.  ', 'nm1.jpg', 'nm1.jpg', 'nm1.jpg', 7, 74, 10),
(78, 'Shoes', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.                                ', 'nm2.jpg', 'nm2.jpg', 'nm2.jpg', 7, 74, 15),
(80, ' Winterized Woven Training Jacket', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.                               ', 'nm3.jpg', 'nm3.jpg', 'nm3.jpg', 7, 74, 52),
(81, 'Nike Women Air Max Thea', '                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'nw1.jpg', 'nw1.jpg', 'nw1.jpg', 7, 75, 80),
(82, 'Sportswear Pants', '                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.', 'nw22.webp', 'nw22.webp', 'nw22.webp', 7, 75, 10),
(83, ' Nike Women | HYPEBAE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.                                ', 'nw3.jpg', 'nw3.jpg', 'nw3.jpg', 7, 75, 50);

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `vendor_id` int(5) NOT NULL,
  `vendor_name` varchar(20) NOT NULL,
  `vendor_email` varchar(20) NOT NULL,
  `vendor_pass` varchar(20) NOT NULL,
  `vendor_image` text NOT NULL,
  `username` varchar(10) NOT NULL,
  `mobile` int(5) NOT NULL,
  `address` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`vendor_id`, `vendor_name`, `vendor_email`, `vendor_pass`, `vendor_image`, `username`, `mobile`, `address`) VALUES
(1, 'Armani', 'capplegarth0@cloudfl', 'z6Bx8Qdk', 'Armani.JPG', 'mdavenall0', 0, '0 Grasskamp Park'),
(2, 'Apple', 'dcouchman1@msn.com', 'kjY6xHn2G', 'Apple.PNG', 'lthurstan1', 86, '0499 Birchwood Avenu'),
(3, 'Samsung ', 'spawellek2@ow.ly', 'XpUmKU1', 'samsung.JPG', 'sstoter2', 86, '97814 Sunbrook Trail'),
(4, 'Huawei ', 'rvittel3@google.co.u', 'gr6moDY30jCN', 'Huawi.PNG', 'jtarling3', 55, '4559 Hoffman Crossin'),
(5, 'Zara', 'ereedick4@4shared.co', 'WTnZ4AHrk', 'Zara.JPG', 'nfrede4', 359, '4630 Dryden Park'),
(6, 'The castle', 'cperle5@blogspot.com', 'RLKP1ss', 'castel.JPG', 'eluxen5', 234, '6999 Golden Leaf Ter'),
(7, 'Nike', 'sbirts6@so-net.ne.jp', 'oBZQl1', 'Nike.PNG', 'ojanes6', 218, '98 Jackson Circle'),
(8, 'Adidas', 'nglidder7@joomla.org', 'Vwj3iaEP', 'adidas.PNG', 'aivanovic7', 62, '493 Lien Crossing'),
(9, 'KFC', 'dlandman8@simplemach', '30LHxRPvK', 'kfc.JPG', 'knassey8', 63, '9892 Kedzie Hill'),
(10, 'McDonald', 'brapp9@acquirethisna', 'zEOct0Vb', 'McDonald.JPG', 'mballham9', 55, '48848 Golden Leaf Pa'),
(21, 'Jaeger-LeCoultre', 'mm@dd.com', 'ssdczc', 'Jaeger-LeCoultre.JPG', '', 77777, 'amman'),
(23, 'Lange & Söhne', 'mm@dd.com', 'ssdczc', 'Lange & Söhne.PNG', '', 21254565, 'irbid'),
(25, 'Audemars Piguet', 'pinky@gmail.com', 'sss1', 'Audemars Piguet.JPG', 'memoh', 888998489, 'USA'),
(26, 'Ally Capellino', 'dsfds@fsdf', 'dfdfs', 'Ally Capellino.PNg', 'dsfsdf', 456454, 'sdfsf');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accept_vendor`
--
ALTER TABLE `accept_vendor`
  ADD PRIMARY KEY (`v_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`),
  ADD KEY `vendor_id` (`vendor_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`f_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `cust_id` (`cust_id`),
  ADD KEY `cat_id` (`cat_id`),
  ADD KEY `pro_id` (`pro_id`),
  ADD KEY `v_id` (`v_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`pro_id`),
  ADD KEY `product_ibfk_1` (`cat_id`),
  ADD KEY `v_id` (`v_id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`vendor_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accept_vendor`
--
ALTER TABLE `accept_vendor`
  MODIFY `v_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `f_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `pro_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `vendor_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`vendor_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`cust_id`) REFERENCES `customer` (`c_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`cat_id`) REFERENCES `category` (`cat_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`pro_id`) REFERENCES `product` (`pro_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_4` FOREIGN KEY (`v_id`) REFERENCES `vendor` (`vendor_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `category` (`cat_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`v_id`) REFERENCES `vendor` (`vendor_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
